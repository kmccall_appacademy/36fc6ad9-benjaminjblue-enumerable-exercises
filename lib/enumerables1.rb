# EASY

# Define a method that returns an array of only the even numbers in its argument
# (an array of integers).
def get_evens(arr)
    arr.select{|n| n.even?}
end

# Define a method that returns a new array of all the elements in its argument
# doubled. This method should *not* modify the original array.
def calculate_doubles(arr)
    arr.map{|x| x * 2}
end

# Define a method that returns its argument with all the argument's elements
# doubled. This method should modify the original array.
def calculate_doubles!(arr)
    arr.map!{|x| x * 2}
end

# Define a method that returns the sum of each element in its argument
# multiplied by its index. array_sum_with_index([2, 9, 7]) => 23 because (2 * 0) +
# (9 * 1) + (7 * 2) = 0 + 9 + 14 = 23
def array_sum_with_index(arr)
    arr.each_with_index.reduce(0){|tot, (n, i)| tot + n * i}
end

# MEDIUM

# Given an array of bids and an actual retail price, return the bid closest to
# the actual retail price without going over that price. Assume there is always
# at least one bid below the retail price.
def price_is_right(bids, actual_retail_price)
    bids.reduce(-1.0 / 0.0) do |closest, bid|
        not_too_bid = 0 <= actual_retail_price - bid
        closer = actual_retail_price - bid < actual_retail_price - closest
        not_too_bid && closer ? bid : closest
    end
end

# Given an array of numbers, return an array of those numbers that have at least
# n factors (including 1 and the number itself as factors).
# at_least_n_factors([1, 3, 10, 16], 5) => [16] because 16 has five factors (1,
# 2, 4, 8, 16) and the others have fewer than five factors. Consider writing a
# helper method num_factors

def num_factors(number)
    (1..number/2).reduce([number]) {|factors, x| number % x == 0 ? factors << x : factors}
end

def at_least_n_factors(numbers, n)
    numbers.select{|num| num_factors(num).length >= n}
end

# HARD

# Define a method that accepts an array of words and returns an array of those
# words whose vowels appear in order. You may wish to write a helper method:
# ordered_vowel_word?

def ordered_vowel_word?(word)
    hash = {}
    'aeiou'.each_char.with_index {|ch, i| hash[ch] = hash[ch.upcase] = i }
    word.each_char.reduce(0) do |index, ch|
        if hash[ch]
            if hash[ch] >= index
                hash[ch]
            else
                return false
            end
        else
            index
        end
    end
    true
end

def ordered_vowel_words(words)
    words.select{|word| ordered_vowel_word?(word)}
end

# Given an array of numbers, return an array of all the products remaining when
# each element is removed from the array. You may wish to write a helper method:
# array_product.

# products_except_me([2, 3, 4]) => [12, 8, 6], where: 12 because you take out 2,
# leaving 3 * 4. 8, because you take out 3, leaving 2 * 4. 6, because you take out
# 4, leaving 2 * 3

# products_except_me([1, 2, 3, 5]) => [30, 15, 10, 6], where: 30 because you
# take out 1, leaving 2 * 3 * 5 15, because you take out 2, leaving 1 * 3 * 5
# 10, because you take out 3, leaving 1 * 2 * 5 6, because you take out 5,
# leaving 1 * 2 * 3
def products_except_me(numbers)
    #if there is more than one 0 
    product = numbers.drop(1).reduce(:*)
    if product == 0
        zero_index, r_zero_index = numbers.index(0), numbers.rindex(0)
        if zero_index != r_zero_index
            return Array.new(numbers.length, 0)
        else
            res = Array.new(numbers.length, 0)
            res[zero_index] = (numbers[0...zero_index] + numbers[zero_index + 1..-1]).reduce(:*)
            return res
        end
    elsif numbers[0] == 0
        return [product] + Array.new(numbers.length - 1, 0)
    end
    return [product] + numbers.drop(1).map.with_index do |num, i|
        product = product * numbers[i] / num
    end
end