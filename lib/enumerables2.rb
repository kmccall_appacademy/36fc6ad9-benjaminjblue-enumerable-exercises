require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
    arr.reduce(0, :+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method

def sub_string?(str, sub)
    i = 0
    str.each_char do |ch|
        i += 1 if ch == sub[i]
        return true if i >= sub.length
    end
    false
end

def in_all_strings?(long_strings, substring)
    long_strings.all?{|str| sub_string?(str, substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
    alpha = Array.new(26,0)
    a_ord = 'a'.ord
    string.each_char {|ch| alpha[ch.ord - a_ord] += 1 if ch != ' ' }
    letters = alpha.map.with_index {|count, i| count > 1 ? (a_ord + i).chr : nil }.compact
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!

def longest_two_words(string)
    all_words = string.split
    all_words.drop(2).reduce(all_words[0,2]) do |longest_2, word|
        words = (longest_2 + [word])
        min_word = words.reverse.min { |a, b| a.length <=> b.length }
        i = words.rindex(min_word)
        words[0...i] + words[i + 1..-1]
    end
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
    #doesn't get simpler than that!
    ('a'..'z').to_a - string.chars.uniq
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
    #this can't be the best way but one line means save time (not for the computer, for me!)
    (first_yr..last_yr).select {|yr| yr.to_s.chars.uniq.length == yr.to_s.length }
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
    #not terribly fast but it is nice looking
    repeats = songs.drop(1).select.with_index {|song, i| song == songs[i]}
    (songs - repeats).uniq
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
    #why deal with punctuation if I have to ignore it!
    string = string.delete('.,!?;:/"\'-`()[]{}')

    words = string.split
    index = nil

    #grab first word with a 'c'
    words.each_with_index.any? {|str, i| index = i if str.include?('c') }

    #or maybe not, if not say bye
    return '' if index == nil

    current_word = words[index]

    #yeah so what dist is off by one? who asked you?!?
    #it doesn't matter though, it won't have any affect on the answer
    dist = current_word.length - current_word.rindex('c')

    #compare c indices of each word, replace dist and current_word if there is a new winner
    string.split.each do |word|
        i = word.rindex('c')
        if i && word.length - i < dist
            current_word = word
            dist = word.length - i
        end
    end
    current_word
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
    #gets answer with only one pass through, that is why it isn't very concise
    res = []
    num = arr[0]
    first_index = last_index = 0
    i = 1
    while i < arr.length
        if arr[i] == arr[i - 1]
            last_index = i
        else
            if first_index != last_index
                res << [first_index, last_index]
            end
            num = arr[i]
            first_index = last_index = i
        end
        i += 1
    end

    if first_index != last_index
        res << [first_index, last_index]
    end
    res
end